const mongoose = require('mongoose');

const { Schema } = mongoose;


const ShorterSchema = new Schema({
  code: String,
  url: String,
},
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

module.exports = mongoose.model('Shorter', ShorterSchema);
