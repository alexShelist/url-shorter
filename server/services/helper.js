const request = require("request");
const URL = require('url-parse');

const isValidUrl = (url) => {
  // const options = new URL(url);
  const options = {
    url,
    headers: {
      'User-Agent': 'request'
    }
  };
  return new Promise(function(resolve, reject) {
    request.get(options, function(err, resp, body) {
      if (err) {
        reject(err);
      } else {
        resolve(resp);
      }
    })
  });
};

module.exports = {
  isValidUrl,
};
