const express = require('express');
const router = express.Router();
const controller = require('../../controllers/urlShorterController');

router.post('/', controller.addNewLink);
router.get('/count/fields', controller.getCountShorter);
router.get('/:code', controller.getShorter);

module.exports = router;
