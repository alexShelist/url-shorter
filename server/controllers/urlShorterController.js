const Shorter = require('../models/Shorter');
const shortid = require("shortid");

const { isValidUrl } = require('../services/helper');


const getShorter = (req, res) => {
  const { code } = req.params;
  return Shorter.findOne({ code })
    .then((short) => res.json({ url: short.url }))
    .catch((err) => res.status(500).json({err: 'SERVER_ERROR'}));
}

const getCountShorter = (req, res) => {
  return Shorter.count()
    .then((count) => res.json({ count }))
    .catch((err) => res.status(500).json({err: 'SERVER_ERROR'}));
}

const addNewLink = (req, res) => {
  const { url, code } = req.body;
  isValidUrl(url)
    .then(() => {
    if (!code) {
      return Shorter.findOne({url})
        .then((link) => {
          if (link) {
            return res.json({code: `${process.env.FRONTEND_URL}/${link.code}`});
          }
          const code = shortid.generate();
          const short = new Shorter({
            url,
            code,
          });
          short.save((err) => {
            if (err) {
              return res.status(500).json({err: 'SERVER_ERROR'});
            }
            return res.json({code: `${process.env.FRONTEND_URL}/${code}`});
          });
        })
        .catch((err) => res.status(500).json({err: 'SERVER_ERROR'}));
    }
      return Shorter.findOne({ code })
      .then((link) => {
      if (link) {
        if (link.url === url) {
          res.json({code: `${process.env.FRONTEND_URL}/${code}`});
        }
        return res.json({codeError: 'This code is already in use. Please choose another'});
      }
        const short = new Shorter({
          url,
          code,
        });
        short.save((err) => {
          if (err) {
            return res.status(500).json({err: 'SERVER_ERROR'});
          }
          res.json({code: `${process.env.FRONTEND_URL}/${code}`});
        });
      })
    })
    .catch((err) => res.status(400).json({err: 'URL_NOT_VALID'}));
}

const checkCodeAndRedirect = (req, res) => {
  const { code } = req.params;
  Shorter.findOne({ code })
    .then((short) => {
    if (short) {
      return res.redirect(short.url);
    }
    return res.redirect('/');
  })
    .catch((err) => res.redirect('/'));
}


module.exports = {getShorter, addNewLink, checkCodeAndRedirect, getCountShorter};
