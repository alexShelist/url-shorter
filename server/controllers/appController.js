const Shorter = require('../models/Shorter');

const deleteOldLinks = () => {
  let date = new Date();
  date.setDate(date.getDate() - 15);
  Shorter.deleteMany({
    createdAt: {
      $lte: date,
    }
  }).then((result) => {
    console.log(result);
  })
}

module.exports = { deleteOldLinks };
