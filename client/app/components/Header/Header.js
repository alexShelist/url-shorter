import React from 'react';

import { Link } from 'react-router-dom';

const Header = () => (
  <header>
      <h1 className="text-center text-muted"> Link shorter </h1>


      <hr />
  </header>
);

export default Header;
