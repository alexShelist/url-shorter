import React, {Component} from 'react';
import 'whatwg-fetch';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      link: '',
      result: '',
      loading: '',
      error: '',
      code: '',
      codeError: '',
      count: 0,
    };
  }

  componentDidMount() {
    this.checkCount();
  }

  handleChange = ({target: { id, value}}) => {
    this.setState({[id]: value})
  };

  checkStatusFetch = (response) => {
    if (response.status >= 200 && response.status < 300) {
      return response
    } else {
      let error = new Error(response.status)
      error.response = response
      throw error
    }
  }

  generateShortUrl = () => {
    const { link, loading, code } = this.state;
    if (!loading && this.validateForm()) {
      this.setState({ loading: true, result: '' });
      fetch('/api/shorter', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          url: link,
          code,
        })
      })
        .then(this.checkStatusFetch)
        .then(res => res.json())
        .then(({ code, codeError }) => {
          this.checkCount();
          this.setState({ result: code, error: '', loading: false, codeError });
        })
        .catch(() => {
          this.setState({error: "Link not valid, chek it please", loading: false});
        });
    }
  };

  checkCount = () => {
      fetch('/api/shorter/count/fields', { method: 'GET' })
        .then(this.checkStatusFetch)
        .then(res => res.json())
        .then(({ count }) => {
          this.setState({ count });
        })
        .catch(() => {

        });
  };

  validateForm = () => {
    const {link } = this.state;
    if (link.trim().length === 0) {
      this.setState({error: 'Link is required'})
      return 0;
    }
    if (link.indexOf('http') !== 0) {
      this.setState({error: "Link must be start from 'http' or 'https' "});
      return 0;
    }
    this.setState({error: '', codeError: ''});
    return 1;
  };

  render() {
    const {link, result, loading, error, count, code, codeError} = this.state;
    return (
      <div className="container">
        <h6>We have {count} links in our Database</h6>
        <div className="form-group">
          <label htmlFor="link">Input your link here. </label>
          <input className={`form-control${error ? ' is-invalid' : ''}`} value={link} onChange={this.handleChange}
                 id="link" placeholder="Enter link"/>
          {error ? <div className="invalid-feedback">
              {error}
            </div> :
            <small id="emailHelp" className="form-text text-muted">The link must be valid and start with "http" or "
              https"</small>}
        </div>
        <div className="form-group">
          <label htmlFor="link">Input code if tou wonna special link </label>
          <input className={`form-control${codeError && code ? ' is-invalid' : ''}`} value={code} onChange={this.handleChange}
                 id="code" placeholder="Enter code"/>
          { code && !codeError && <small id="emailHelp" className="form-text text-muted">The link will look like {`${process.env.FRONTEND_URL}/${code}`}</small>}
          { code && codeError && <div className="invalid-feedback">{codeError}</div>}
        </div>
        <div className="form-group">
          <label htmlFor="result">Short link</label>
          <input value={result} className="form-control" id="result" placeholder="Result will be here"/>
        </div>
        <button type="submit" onClick={this.generateShortUrl} disabled={loading} className="btn btn-primary">
          {loading ? 'Loading...' : 'Generate'}
        </button>
      </div>
    );
  }
}

export default Home;
