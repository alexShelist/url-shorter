import React from 'react';

const Footer = () => (
  <footer>
      <hr />
      <p className="text-center text-muted">© Copyright 2018 Aleksandr Shelist</p>
  </footer>
);

export default Footer;
