# url-shorter
The project to create short url addresses. Please note, the life of a short link-15 days


The project is developed and tested on the platform `node.js`

###Start command:

* `npm run start:dev` - running the project in development mode

###Test server:

* [`http://34.228.199.128/`](http://34.228.199.128/)

###Run project:

* install [`mongodb`](https://docs.mongodb.com/manual/administration/install-community/)
* run - `npm i`
* add `.env`
* run mongodb - `sudo mongod`
* run project - ``npm run start:dev`

### Configuration
.env
```javascript
FRONTEND_URL=....
NODE_ENV=development|production
PORT=8080
MONGODB_URL=mongodb://user:password@host:port/dbName
```

###Api list (! - required param):

* `post(/api/shorter, {url: String!, code: String})` - add new associate beetwen url and code. If code empty, api generate random code return {code: string} or Error
* `get(/api/shorter/count/fields)` - get count field in database, return {code: String} or Error return
* `get(/api/shorter/:code)` - get url by associate code, return {url: String} or Error return

###Route

* `get(/:code)` - redirect user to url, which associated with `code`. If haven't associated url, redirect to homepage


